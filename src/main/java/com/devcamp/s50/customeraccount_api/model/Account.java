package com.devcamp.s50.customeraccount_api.model;

public class Account {
    private int id;
    private Customer customer;
    private double balance = 0;
    
    public Account(int id, Customer customer, double balance) {
        this.id = id;
        this.customer = customer;
        this.balance = balance;
    }

    public Account(int id, Customer customer) {
        this.id = id;
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        double roundOff = Math.round(balance*100)/100; //làm tròn đến 2 chữ số thập phân
        return this.customer.getName() + "(" + this.id + ")" + " balance= " + roundOff;

    }
    
}
