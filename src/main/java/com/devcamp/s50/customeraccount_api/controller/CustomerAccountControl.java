package com.devcamp.s50.customeraccount_api.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.customeraccount_api.model.*;

@RestController
public class CustomerAccountControl {
    @CrossOrigin
    @GetMapping("/accounts")
    public ArrayList<Account> getListAccount(){
        Customer customer1 = new Customer(01, "Tran Van A", 8);
        Customer customer2 = new Customer(02, "Tran Van C", 12);
        Customer customer3 = new Customer(03, "Tran Van D", 12);
        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3);

        Account account1 = new Account(777, customer1);
        Account account2 = new Account(888, customer2);
        Account account3 = new Account(999, customer3);
        System.out.println(account1);
        System.out.println(account2);
        System.out.println(account3);

        ArrayList<Account> listAccount = new ArrayList<>();
        listAccount.add(account1);
        listAccount.add(account2);
        listAccount.add(account3);
        return listAccount;
    }
}
